-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  ven. 03 juil. 2020 à 06:51
-- Version du serveur :  5.7.26
-- Version de PHP :  7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `releaserbdd`
--

-- --------------------------------------------------------

--
-- Structure de la table `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_64C19C15E237E06` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `category`
--

INSERT INTO `category` (`id`, `name`, `description`) VALUES
(25, 'BDD', 'à compléter'),
(26, 'SQL', 'à compléter'),
(27, 'Front', 'à compléter'),
(28, 'CatalogMassUp', 'à compléter'),
(29, 'www.pecheur.com', 'à compléter'),
(30, 'ExecV3', 'à compléter'),
(31, 'chasseur.pecheur.com', 'à compléter'),
(32, 'admin.pecheur.com', 'à compléter'),
(33, 'SAB', 'à compléter'),
(34, 'LAVCO', 'à compléter'),
(35, 'Vortex', 'à compléter'),
(36, 'ExportPartenaire', 'à compléter'),
(37, 'Task Launcher', 'à compléter'),
(38, 'Secure', 'à compléter'),
(39, 'p32devise.pecheur.com', 'à compléter'),
(40, 'CMU', 'à compléter'),
(41, 'ExactTarget', 'à compléter'),
(42, 'extranet.pecheur.com', 'à compléter'),
(43, 'Etiquette Printer', 'à compléter'),
(44, 'PecheurExpeditions', 'à compléter'),
(45, 'api.pecheur.com', 'à compléter'),
(46, 'Application', 'à compléter'),
(47, 'ABTasty', 'à compléter'),
(48, 'Serveur / Infrastructure', 'à compléter'),
(49, 'A TRIER', 'a compléter');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
