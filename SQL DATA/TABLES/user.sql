-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  ven. 03 juil. 2020 à 06:51
-- Version du serveur :  5.7.26
-- Version de PHP :  7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `releaserbdd`
--

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles` json NOT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  `guid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `api_token` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8D93D649F85E0677` (`username`),
  UNIQUE KEY `UNIQ_8D93D649E7927C74` (`email`),
  UNIQUE KEY `UNIQ_8D93D6497BA2F5EB` (`api_token`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `username`, `email`, `roles`, `password`, `is_deleted`, `guid`, `api_token`) VALUES
(7, 'ADMIN RELEASER', 'AdminReleaser@pecheur.com', '[\"ROLE_ADMIN\"]', '$2y$13$8namQNHoeYnAyG7arzDdneuQK6VsZv0qltJcnhA0B.AANAvPATq9i', 0, NULL, 'F3F30F7D-4332-4152-965F-AFA3AF481D7C'),
(8, 'Esteban RESTOY', 'e.restoy@fugam.lan', '[\"ROLE_ADMIN\"]', NULL, 0, NULL, 'ECE28F82-9F87-495B-AED1-C20705110F03'),
(9, 'Alexandre RUIZ', 'a.ruiz@fugam.lan', '[\"ROLE_ADMIN\"]', NULL, 0, NULL, 'F77FCD90-DBB3-4240-B72D-950476A47F53'),
(10, 'Joël VERCAUTEREN', 'j.vercauteren@fugam.lan', '[\"ROLE_ADMIN\"]', NULL, 0, NULL, '67735714-125E-49FA-BE74-220AADC1CB40'),
(11, 'Sylvain IGLESIAS', 's.iglesias@fugam.lan', '[\"ROLE_ADMIN\"]', NULL, 0, NULL, 'EEF6AF7E-1EDB-4A2C-8AF6-7A9B3B95096E'),
(12, 'Mourad BAHLOUL', 'm.bahloul@fugam.lan', '[\"ROLE_ADMIN\"]', NULL, 0, NULL, 'C1F734FB-7B19-42AA-BD30-0E449E476ADB'),
(13, 'Olivier ARGENTIERI', 'o.argentieri@fugam.lan', '[\"ROLE_ADMIN\"]', NULL, 0, NULL, '3673AD75-E331-4A48-975F-D00BB38BB1F5'),
(14, 'Cécile DUTEMPS', 'c.dutemps@fugam.lan', '[\"ROLE_ADMIN\"]', NULL, 0, NULL, '24DAB670-942C-495F-B845-D5881DAFA174'),
(15, 'NiKo BRUNET', 'n.brunet@fugam.lan', '[\"ROLE_ADMIN\"]', NULL, 0, NULL, 'D457D9F7-68B6-4732-A64C-FD2AD8ECC9B3');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
