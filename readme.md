## Releaser

Releaser est développé pour l'entreprise Fugam qui possède Pecheur.com et Chasseur.com.
L'objectif de l'application est de faciliter la consultation de l'historique des outils utilisés par l'entreprise car la solution actuellement utilisée (un mail/excel contenant les differentes majs) n'est pas la plus optimisée. 
Ce projet a été développé à l'aide de Symfony.

### Prérequis

> Vous devez avoir npm installé sur votre machine  
```sh
$ curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
$ sudo apt install nodejs
```

> Vous devez avoir gulp-cli installé sur votre machine  
```sh
$ npm install --global gulp-cli
```

### Installation

Si vous souhaitez developper il vous faudra effectuer ces commandes pour installer le projet :

```sh
$ composer install
$ npm install
$ gulp copyCssJs
```

Si vous avez bien effectuez ces commandes vous devez apercevoir cela dans votre terminal :
```sh
[14:54:27] Starting 'addBootstrapAndJquery'...
[14:54:27] Finished 'addBootstrapAndJquery' after 201 ms
```
## Configuration

#####MAIL :
Un mail est envoyé quand : 
- Un utilisateur viens d'etre crée (envoi du mail au nouvel utilisateur, le mail contient ses logs)
- Un erreur crtitique s'est produite (un mail est envoyé a une liste d'adresse)

Pour configurer l'adresse de l'envoi du mail il faut modifier la ligne :
```sh
MAILER_DSN=smtp://email:motDePasse@smtp.gmail.com
```
Si vous souhaitez modifier les adresses email qui recoivent les logs d'erreur il faut le faire dans le .env
```sh
ERRORMAILLIST = ["email1","email2"...]
```
    

## Author 
Alexandre Ruiz  
Esteban Restoy
