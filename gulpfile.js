var gulpCopy = require('gulp-copy');
var gulp = require('gulp');
var CSSPath = ['node_modules/bootstrap/**/bootstrap.min.css','node_modules/jquery-datetimepicker/build/jquery.datetimepicker.min.css'];
var JsPath = ['node_modules/bootstrap/**/bootstrap.min.js', 'node_modules/jquery/**/jquery.min.js','node_modules/jquery-datetimepicker/build/jquery.datetimepicker.full.min.js'];

var outputJS = 'public/js';
var outputCSS = 'public/css';

function defaultTask(cb) {
    // place code for your default task here
    cb();
}

function copyCssJs() {
    gulp
        .src(CSSPath)
        .pipe(gulpCopy(outputCSS, {prefix: 10}));

    return gulp
        .src(JsPath)
        .pipe(gulpCopy(outputJS, {prefix: 10}));
}

exports.default = defaultTask;
exports.copyCssJs = copyCssJs;