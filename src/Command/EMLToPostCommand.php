<?php

namespace App\Command;

use App\Repository\CategoryRepository;
use App\Repository\PostRepository;
use App\Repository\UserRepository;
use App\Service\EMLService;
use App\Service\LDAPService;
use App\Service\UserService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class EMLToPostCommand extends Command
{
    protected static $defaultName = 'app:EMLToPost';
    private $EMLService;
    private $userRepository;
    private $postRepository;
    private $categoryRepository;
    private $LDAPService;
    private $userService;

    public function __construct(UserRepository $userRepository, PostRepository $postRepository, EMLService $EMLService, CategoryRepository $categoryRepository, LDAPService $LDAPService, UserService $userService)
    {
        $this->userRepository = $userRepository;
        $this->postRepository = $postRepository;
        $this->EMLService = $EMLService;
        $this->categoryRepository = $categoryRepository;
        $this->LDAPService = $LDAPService;
        $this->userService = $userService;
        parent::__construct();

    }

    protected function configure()
    {
        $this
            ->setDescription('Cette commande permet de transformer des mail .eml en post et les insérer dans la BDD')
            ->addArgument('path', InputArgument::REQUIRED, 'path du dossier des fichiers .eml');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $path = $input->getArgument('path');

        if ($path) {
            $io->note(sprintf('You passed an argument: %s', $path));
        }
        $results = $this->EMLService->EMLFilesProcessCommand($path, $this->userRepository, $this->postRepository, $this->categoryRepository, $this->LDAPService, $this->userService);
        $io->success('You have a new command! Now make it your own! Pass --help to see your options.');

        return 0;
    }
}
