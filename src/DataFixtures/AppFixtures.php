<?php

namespace App\DataFixtures;


use App\Entity\category;
use App\Entity\User;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{

    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }


    public function load(ObjectManager $manager)
    {

        $users = [["Mourade", "m.bahloul@pecheur.com", ["ROLE_ADMIN"]],
            ["Sylvain", "s.iglesias@pecheur.com", ["ROLE_ADMIN"]],
            ["Niko", "niko.brunet@pecheur.com", ["ROLE_ADMIN"]],
            ["Joël", "j.vercauteren@pecheur.com", ["ROLE_ADMIN"]],
            ["Alex R", "a.ruiz@pecheur.com", ["ROLE_ADMIN"]],
            ["Fabien", "f.soeiro@pecheur.com", []],
            ["Jérémy", " j.crouzet@pecheur.com", []],
            ["Faustin", "f.falcon@pecheur.com", []],
            ["Xavier", "x.desvignes@pecheur.com", []],
            ["Alex M", "a.moral@pecheur.com", []],
            ["Raphaël", "visibilite.referencement@gmail.com", []]];

        $categories = ["BDD", 'SQL', "Front", "CatalogMassUp", "www.pecheur.com", "ExecV3", "chasseur.pecheur.com", "admin.pecheur.com", "SAB", "LAVCO", "Vortex", "ExportPartenaire",
            "Task Launcher", "Secure", "p32devise.pecheur.com", "CMU", "ExactTarget", "extranet.pecheur.com", "Etiquette Printer", "PecheurExpeditions", "api.pecheur.com", "Application", "ABTasty", "Serveur / Infrastructure"];

        $posts = [["Ajout GLS italie (#1716)", ["Vortex"], "a.ruiz@pecheur.com", DateTime::createFromFormat('d/m/Y H:i', "20/05/2020 12:10")],
            ["Ajout transporteur GLS Europe (#1716)
Ajout listing + extraction des produits testés par F&T (#1241)", ["admin.pecheur.com"], "s.iglesias@pecheur.com", DateTime::createFromFormat('d/m/Y H:i', "21/05/2020 14:15")],
            ["Ajout Chrono Fresh + GLS italie", ["PecheurExpeditions"], "j.vercauteren@pecheur.com", DateTime::createFromFormat('d/m/Y H:i', "20/05/2020 12:10")],
            ["Retrait encart express/déstockage/nouveautés sur mobile pour l'univers Terroir.", ["www.pecheur.com"], "a.ruiz@pecheur.com", DateTime::createFromFormat('d/m/Y H:i', "22/05/2020 08:10")],
            ["Ajout des compteurs de nb commande et nb réfs pour la sortie Pecheur Complement (#1718)", ["admin.pecheur.com"], "j.vercauteren@pecheur.com", DateTime::createFromFormat('d/m/Y H:i', "15/05/2020 18:05")],
            ["Correction erreur dans les chiffres quotidiens pour pilotage IKOM  #1733 ", ["Task Launcher"], "a.ruiz@pecheur.com", DateTime::createFromFormat('d/m/Y H:i', "10/04/2020 11:15")],
            ["Prise en compte de la boutique PecheurFood pour les mails de conf expé", ["Task Launcher"], "a.ruiz@pecheur.com", DateTime::createFromFormat('d/m/Y H:i', "05/04/2020 20:55")],
            ["Ajout \"Cap\" pour découper une commande fournisseur si il attend le Cap (#1384)", ["admin.pecheur.com"], "j.vercauteren@pecheur.com", DateTime::createFromFormat('d/m/Y H:i', "01/03/2020 12:10")],
            ["Découpage des commandes fournisseur pour qu'elles ne soit pas trop grosse + prise en compte de ce \"Cap\" administrer pour le découpage par bac de la Pecheur  (#1384)", ["Task Launcher"], "j.vercauteren@pecheur.com", DateTime::createFromFormat('d/m/Y H:i', "22/02/2020 22:22")],
            ["Modification du tri par défaut sur les teaser empêchant l'affichage de certains teaser", ["Task Launcher"], "a.ruiz@pecheur.com", DateTime::createFromFormat('d/m/Y H:i', "10/02/2020 00:42")],
            ["Mise en place des opé marketing 
Mise en place du WYSIWYG sur les teasers", ["www.pecheur.com", "chasseur.pecheur.com"], "a.ruiz@pecheur.com", DateTime::createFromFormat('d/m/Y H:i', "04/02/2020 12:10")],
            ["Prise en compte de tous les modèles d'une commande fournisseur pour la prio Stock (#1564)
+ ajout pagination sur listing carte cadeau et listings sur fiche Client", ["admin.pecheur.com"], "j.vercauteren@pecheur.com", DateTime::createFromFormat('d/m/Y H:i', "22/05/2020 12:10")],
            ["Retours clients 
Fix bug Oney (OKKO)", ["www.pecheur.com"], "s.iglesias@pecheur.com", DateTime::createFromFormat('d/m/Y H:i', "20/01/2020 12:10")],
            ["Retours clients ", ["chasseur.pecheur.com"], "s.iglesias@pecheur.com", DateTime::createFromFormat('d/m/Y H:i', "20/05/2019 12:10")],
            ["Gestion des appels à Secure pour les types VOID", ["Secure"], "a.ruiz@pecheur.com", DateTime::createFromFormat('d/m/Y H:i', "20/12/2018 12:10")],
            ["Gestion des codes d'erreur Oney 4XXX, 5XXX et 6XXX pour ne plus recevoir de mails inutiles", ["chasseur.pecheur.com", "www.pecheur.com"], "a.ruiz@pecheur.com", DateTime::createFromFormat('d/m/Y H:i', "10/09/1999 12:10")],
        ];

        /* USERS */
        $usersUseForPosts = [];
        foreach ($users as $user) {
            $u = new User();
            $u->setUsername($user[0]);
            $u->setEmail($user[1]);
            $u->setPassword($this->encoder->encodePassword($u, 'achanger'));
            $u->setRoles($user[2]);
            $usersUseForPosts[$u->getEmail()] = $u;
            $manager->persist($u);
        }
        $manager->flush();
        /* ADMIN RELEASER */
        $u = new User();
        $u->setUsername("ADMIN RELEASER");
        $u->setEmail($_ENV["ADMINEMAIL"]);
        $u->setPassword($this->encoder->encodePassword($u, '$fugam-63-gdf$'));
        $u->setRoles(["ROLE_ADMIN"]);
        $manager->persist($u);

        /* Categories */
        $catsUseForPosts = [];
        foreach ($categories as $category) {
            $c = new Category();
            $c->setName($category);
            $c->setDescription("à compléter");
            $catsUseForPosts[$c->getName()] = $c;
            $manager->persist($c);
        }
        $manager->flush();
        /* Posts */
        /* foreach ($posts as $post) {

             $p = new Post();
             $p->setDescription($post[0]);
             $p->setDate($post[3]);
             foreach ($post[1] as $categoryName) {
                 $p->addCategory($catsUseForPosts[$categoryName]);
             }
             $p->setAuthor($usersUseForPosts[$post[2]]);
             $manager->persist($p);
         }
         $manager->flush();*/
//admin a faire un utilisateur + catégories
        // ajouter link  + mail si elastic search

    }
}
