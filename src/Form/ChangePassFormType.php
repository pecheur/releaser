<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ChangePassFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder = $this->addPasswordField($builder, 'oldPass', 'Ancien mot de passe : ');
        $builder = $this->addPasswordField($builder, 'newPass', 'Nouveau mot de passe : ');
        $builder = $this->addPasswordField($builder, 'newPassConfirm', 'Retaper le nouveau mot de passe :');
        return $builder;

    }

    private function addPasswordField(FormBuilderInterface $builder, string $child, string $label): FormBuilderInterface
    {
        return $builder->add($child, PasswordType::class, [
            'label' => $label,
            'trim' => 'true',
            'required' => 'true',
            'attr' => [
                'class' => 'password form-control'
            ]
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
