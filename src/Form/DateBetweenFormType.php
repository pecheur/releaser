<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DateBetweenFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder = $this->addDateTimeControl($builder, 'dateStart', 'Date de debut : ', 'Debut');
        $builder = $this->addDateTimeControl($builder, 'dateEnd', 'Date de fin : ', 'Fin');
    }

    private function addDateTimeControl(FormBuilderInterface $builder, string $child, string $label, string $placeholder): FormBuilderInterface
    {
        return $builder->add($child, TextType::class, [
            'label' => $label,
            'attr' => [
                'readonly' => 'true',
                'placeholder' => $placeholder,
                'class' => 'datetime form-control'
            ]
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
