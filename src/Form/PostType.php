<?php

namespace App\Form;

use DateTime;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PostType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $data = $options['data'][0];
        $post = $options['data'][1];
        $now = new DateTime();
        if ($post == null) {
            $desc = '';
            $date = $now->format('d/m/Y H:i');
        } else {
            $desc = $post->getDescription();
            $date = $post->getDate()->format('d/m/Y H:i');
        }

        $builder
            ->add('date', TextType::class, [
                'label' => 'Date :',
                'attr' => [
                    'value' => $date,
                    'readonly' => 'true',
                    'class' => 'datetime form-control'
                ]])
            ->add('description', TextareaType::class, [
                'label' => 'Description :',
                'data' => $desc,
                'attr' => [
                    'class' => 'form-control'
                ]])
            ->add('categories', ChoiceType::class, array(
                'attr' => ['class' => 'form-check-input '],
                'choices' => $data,
                'required' => false,
                'expanded' => true,
                'choice_label' => 'name',
                'multiple' => true,
                'choice_value' => 'id',
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
        ]);
    }


}
