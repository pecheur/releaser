<?php

namespace App\Form;

use App\Entity\Category;
use App\Entity\Tool;
use App\Entity\Version;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CategoriesAndDatesFormType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $data = $options['data'];
        $builder
            ->add('categories', ChoiceType::class, array(
                'choices' => $data,
                'required' => false,
                'expanded' => true,
                'choice_label' => 'name',
                'multiple' => true,
                'choice_value' => 'id',
                'choice_attr' => function(?Category $category) {
                    return $category ? ['description' => ($category->getDescription())] : [];
                },

            ))
            ->setMethod('GET')
            ->add('description', TextType::class, array(
                'required' => false,

            ))
            ->add('date', DateBetweenFormType::class);

    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
        ]);
    }
}
