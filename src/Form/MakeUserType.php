<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MakeUserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        return $builder
            ->add('email', EmailType::class, [
                'label' => 'Email :',
                'required' => true,
                'attr' => [
                    'placeholder' => "nouvelUser@pecheur.com",
                    'class' => 'email form-control'
                ]
            ])
            ->add('username', TextType::class, [
                'label' => 'Username :',
                'required' => true,
                'attr' => [
                    'placeholder' => "notch",
                    'class' => 'email form-control'
                ]
            ])
            ->add('isAdmin', CheckboxType::class, [
                'label' => 'Admin :',
                'required' => false,
                'attr' => [
                    'placeholder' => "notch",
                    'class' => 'form-check-input"'
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
