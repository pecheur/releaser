<?php

namespace App\Security;

use App\Entity\User;
use App\Repository\UserRepository;
use App\Service\LDAPService;
use App\Service\MailService;
use App\Service\UserService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\Security\Guard\Authenticator\AbstractFormLoginAuthenticator;
use Symfony\Component\Security\Guard\PasswordAuthenticatedInterface;
use Symfony\Component\Security\Http\Util\TargetPathTrait;

class AppAuthenticator extends AbstractFormLoginAuthenticator implements PasswordAuthenticatedInterface
{
    use TargetPathTrait;

    public const LOGIN_ROUTE = 'app_login';

    private $entityManager;
    private $urlGenerator;
    private $csrfTokenManager;
    private $passwordEncoder;
    private $LDAP;
    private $userRepository;
    private $userService;
    private $isLDAPUser = false;
    private $mailService;
    private $mailer;

    public function __construct(EntityManagerInterface $entityManager, UrlGeneratorInterface $urlGenerator, CsrfTokenManagerInterface $csrfTokenManager, UserPasswordEncoderInterface $passwordEncoder, LDAPService $LDAP, UserService $uc, UserRepository $userRepository, MailerInterface $mailer, MailService $mailService)
    {
        $this->entityManager = $entityManager;
        $this->urlGenerator = $urlGenerator;
        $this->csrfTokenManager = $csrfTokenManager;
        $this->passwordEncoder = $passwordEncoder;
        $this->LDAP = $LDAP;
        $this->userRepository = $userRepository;
        $this->userService = $uc;
        $this->mailService = $mailService;
        $this->mailer = $mailer;

    }

    public function supports(Request $request)
    {
        return self::LOGIN_ROUTE === $request->attributes->get('_route')
            && $request->isMethod('POST');
    }

    public function getCredentials(Request $request)
    {
        $credentials = [
            'username' => $request->request->get('username'),
            'password' => $request->request->get('password'),
            'csrf_token' => $request->request->get('_csrf_token'),
        ];
        $request->getSession()->set(
            Security::LAST_USERNAME,
            $credentials['username']
        );

        return $credentials;
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $token = new CsrfToken('authenticate', $credentials['csrf_token']);
        if (!$this->csrfTokenManager->isTokenValid($token)) {
            throw new InvalidCsrfTokenException();
        }
        $user = $this->LDAP->login($credentials['username'], $credentials['password'], $this->userRepository, $this->userService, $this->mailService, $this->mailer);

        if (!$user) {
            $user = $this->entityManager->getRepository(User::class)->findOneBy(['username' => $credentials['username']]);
            if (!$user) {
                $user = $this->entityManager->getRepository(User::class)->findOneBy(['email' => $credentials['username']]);
                if ($user == null) {
                    throw new CustomUserMessageAuthenticationException('Aucune combinaison n\'a été trouvée');
                }
            }
        } else {
            $this->isLDAPUser = true;
        }
        $credentials['username'] = $user->getUsername();

        if ($user->getGuid() != null) {
            throw new CustomUserMessageAuthenticationException('Cet utilisateur n\'est pas inscrit totalement. Veuillez cliquer sur le lien qui vous a été envoyé sur votre boite mail');
        }
        return $user;
    }

    public function checkCredentials($credentials, UserInterface $user)
    {

        if ($this->isLDAPUser) {
            return true;
        }
        return $this->passwordEncoder->isPasswordValid($user, $credentials['password']);
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     */
    public function getPassword($credentials): ?string
    {
        if ($this->isLDAPUser) {
            return null;
        }
        return $credentials['password'];
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        if ($token->getUser()->getIsDeleted()) {
            $token->setAuthenticated(false);
            return new RedirectResponse($this->urlGenerator->generate('disabled'));
        }

        if ($targetPath = $this->getTargetPath($request->getSession(), $providerKey)) {

            return new RedirectResponse($targetPath);
        }


        return new RedirectResponse($this->urlGenerator->generate('posts'));
    }

    protected function getLoginUrl()
    {
        return $this->urlGenerator->generate(self::LOGIN_ROUTE);
    }
}
