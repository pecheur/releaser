<?php

namespace App\Controller;

use App\Form\ChangePassFormType;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserController extends AbstractController
{
    /**
     * @Route("/profil", name="profil", methods={"GET"})
     */
    public function profil(): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        return $this->render('profil/index.html.twig', [
            'controller_name' => 'ProfilController',
        ]);
    }

    /**
     * @Route("/register", name="registerGUID")
     */
    public function registerGUID(Request $request, UserRepository $userRepository, UserPasswordEncoderInterface $passwordEncoder): Response
    {
        if ($request->query->get('guid') == "" || $request->query->count() != 1) {
            return new Response("<h1>Requete invalide</h1>"); // pas trad
        }
        $email = urldecode($request->query->get('email'));
        $key = urldecode($request->query->get('guid'));
        $user = $userRepository->findBy(array("guid" => $key));
        if ($user == null) {
            return new Response("<h1>clé invalide</h1>"); // pas trad
        }
        $form = $this->createFormBuilder()
            ->add('password', PasswordType::class)
            ->add('verificationPassword', PasswordType::class)
            ->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            $password = $form['password']->getData();
            $passwordValidation = $form['verificationPassword']->getData();
            if ($password != $passwordValidation) {
                return new Response("<h1>les mot de passe doivent être identiques !</h1>"); // pas trad
            } else {
                $userRepository->upgradePassword($user[0], $passwordEncoder->encodePassword($user[0], $password));
                return $this->redirectToRoute("app_login");
            }
        }
        return $this->render('profil/register.html.twig', [
            'controller_name' => 'ProfilController',
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/profil/changePassword", name="changePassword")
     */
    public function changePassword(Request $request, UserPasswordEncoderInterface $passwordEncoder, UserRepository $userRepository): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');

        $form = $this->createForm(ChangePassFormType::class);
        $form->handleRequest($request);
        if (!$form->isSubmitted() || !$form->isValid())
            return $this->makeRender(false, null, $form);
        $formData = $form->getData();
        $oldPass = $formData['oldPass'];
        $newPass = $formData['newPass'];
        $confirm = $formData['newPassConfirm'];

        $error = $this->checkFields($oldPass, $newPass, $confirm);
        if ($error != null)
            return $this->makeRender(false, $error, $form);

        $error = $this->changePassRequest($oldPass, $newPass, $userRepository, $passwordEncoder);

        return $this->makeRender(($error == null), $error, $form);
    }

    private function makeRender(bool $passchanged, ?string $error, FormInterface $form): Response
    {
        return $this->render('profil/changePassword.html.twig', [
            'controller_name' => 'ProfilController', 'form' => $form->createView(), 'error' => $error, 'passchanged' => $passchanged,
        ]);
    }

    private function checkFields(string $oldPass, string $newPass, string $newPassConfirm): ?string
    {
        if ($newPass != $newPassConfirm)
            return $error = "les mots de passes ne sont pas identiques";

        if ($newPass == $oldPass)
            return $error = "l'ancien mot de passe et le nouveau sont identique !";

        if (trim($newPass) == '')
            return $error = "Le nouveau mot de passe n'est pas valide";

        return null;
    }

    private function changePassRequest(string $oldPass, string $newPass, UserRepository $userRepository, UserPasswordEncoderInterface $passwordEncoder): ?string
    {
        if ($passwordEncoder->isPasswordValid($this->getUser(), $oldPass)) {
            $userRepository->upgradePassword($this->getUser(), $passwordEncoder->encodePassword($this->getUser(), $newPass));
            return null;
        }

        return $error = "L'ancien mot de passe renseigné est incorrecte";
    }

    /**
     * @Route("/profil/changeEmail", name="changeEmail")
     */
    public function changeEmail(Request $request, UserRepository $userRepository): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');

        $form = $this->createFormBuilder()
            ->add('email', EmailType::class, ['attr' => ['class' => 'email form-control']])
            ->getForm();

        $form->handleRequest($request);
        if (!$form->isSubmitted() || !$form->isValid()) {
            return $this->render('profil/changeEmail.html.twig', [
                'controller_name' => 'ProfilController', 'form' => $form->createView(), 'error' => null, 'emailChanged' => false,
            ]);
        }

        $result = $userRepository->changeEmail($this->getUser(), $form->getData()['email']);

        $error = 'Email deja existant !';
        $emailChanged = false;
        if ($result) {
            $error = null;
            $emailChanged = true;
        }
        return $this->render('profil/changeEmail.html.twig', [
            'controller_name' => 'ProfilController', 'form' => $form->createView(), 'error' => $error, 'emailChanged' => $emailChanged,
        ]);
    }

    /**
     * @Route("/profil/changeIsNotify", name="changeIsNotify")
     */
    public function changeIsNotify(Request $request, UserRepository $userRepository): Response
    {
        $userRepository->changeIsNotify($this->getUser());

        return $this->redirectToRoute("profil");
    }
}