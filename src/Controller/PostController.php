<?php

namespace App\Controller;

use App\Exception\PostException;
use App\Form\CategoriesAndDatesFormType;
use App\Repository\CategoryRepository;
use App\Repository\PostRepository;
use App\Service\CSVService;
use App\Service\PDFService;
use App\Service\XMLService;
use DateTime;
use Doctrine\ORM\QueryBuilder;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\PaginatorInterface;
use SimpleXMLElement;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/posts")
 */
class PostController extends AbstractController
{
    /**
     * @Route("/", name="posts", methods={"GET"})
     */
    public function posts(Request $request, PostRepository $postRepository, CategoryRepository $categoryRepository, PaginatorInterface $paginator, PDFService $PDFService, CSVService $CSVService,XMLService $XMLService): Response
    {
        return $this->postsByCategories($request, $postRepository, $categoryRepository, $paginator, $PDFService,$CSVService, $XMLService);
    }

    /**
     * @Route("?categories={categoryID}", name="postsByCategories")
     */
    public function postsByCategories(Request $request, PostRepository $postRepository, CategoryRepository $categoryRepository, PaginatorInterface $paginator, PDFService $PDFService,CSVService $CSVService,XMLService $XMLService): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        $error = null;
        $nbPage = 9;
        $listeForselect = $categoryRepository->findAll();
        $form = $this->createForm(categoriesAndDatesFormType::class, $listeForselect);

        $form->add('exportPDF', SubmitType::class, ['label' => 'PDF', 'row_attr' => ['class' => 'text-center mt-5 '],
            'attr' => [
                'class' => 'dropdown-item categories_and_dates_form_export'
            ]]);

        $form->add('exportCSV', SubmitType::class, ['label' => 'CSV', 'row_attr' => ['class' => 'text-center mt-5 '],
            'attr' => [
                'class' => 'dropdown-item categories_and_dates_form_export'
            ]]);

        $form->add('exportXML', SubmitType::class, ['label' => 'XML', 'row_attr' => ['class' => 'text-center mt-5 '],
            'attr' => [
                'class' => 'dropdown-item categories_and_dates_form_export'
            ]]);

        $pagination = $this->makePagination($request, $postRepository->findBy(array(), array('date' => 'DESC')), $paginator, $nbPage);
        $form->handleRequest($request);

        // si c'ets le btn export qui a submit alors on change le nb de post par page pour pouvoir les exporter
        if ($form->getClickedButton() === $form->get('exportPDF') || $form->getClickedButton() === $form->get('exportCSV') || $form->getClickedButton() === $form->get('exportXML')) {
            $nbPage = '99999';
        }

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $result = $this->PostFieldsProcess($form->getData()['date']['dateStart'], $form->getData()['date']['dateEnd'], $form->getData()['categories'], $postRepository, $form->getData()['description']);
            } catch (PostException $e) {
                return $this->makeRender($pagination, $e->getMessage(), $form, $PDFService,$CSVService, $XMLService);
            }

            $pagination = $this->makePagination($request, $result, $paginator, $nbPage);

            return $this->makeRender($pagination, null, $form, $PDFService,$CSVService, $XMLService);
        } else {
            if ($request->get('categoryID') != null) {
                $form->get('categories')->submit($request->get('categoryID'));
                $pagination = $this->makePagination($request, $postRepository->findbyCategoryID($request->get('categoryID')), $paginator, $nbPage);

                return $this->makeRender($pagination, $error, $form, $PDFService,$CSVService, $XMLService);
            }

            return $this->makeRender($pagination, $error, $form, $PDFService,$CSVService, $XMLService);
        }
    }

    private function makePagination(Request $request, array $queryBuilder, PaginatorInterface $paginator, int $nbPage): PaginationInterface
    {
        return $paginator->paginate(
            $queryBuilder, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            $nbPage /*limit per page*/
        );
    }

    private function PostFieldsProcess(?string $inputDateStart, ?string $inputDateEnd, array $selectedCategories, postRepository $postRepository, ?string $keyword): array
    {
        $queryBuilder = $this->datesProcess($inputDateStart, $inputDateEnd, $postRepository);
        $queryBuilder = $this->categoriesProcess($queryBuilder, $selectedCategories, $postRepository);
        $queryBuilder = $this->descriptionProcess($queryBuilder, $keyword, $postRepository);
        $queryBuilder->orderBy('p.date', 'DESC');

        return $queryBuilder->getQuery()->getResult();
    }

    private function datesProcess(?string $inputDateStart, ?string $inputDateEnd, PostRepository $postRepository): QueryBuilder
    {
        $dateStart = DateTime::createFromFormat('d/m/Y H:i', $inputDateStart);
        $dateEnd = DateTime::createFromFormat('d/m/Y H:i', $inputDateEnd);
        $error = $this->getDateMessageError($dateStart, $dateEnd);

        if ($error != null)
            throw new PostException($error);

        return $postRepository->findbyBetween2Dates($dateStart, $dateEnd);
    }

    private function getDateMessageError($dateStart, $dateEnd): ?string
    {
        if (!$dateStart xor !$dateEnd) {
            return "Veuillez remplir les deux champs pour la date";
        }

        if ($dateEnd < $dateStart) {
            return "la date deb doit etre anterieur a la date fin";
        }

        return null;
    }

    private function categoriesProcess(QueryBuilder $queryBuilder, array $categories, PostRepository $postRepository): QueryBuilder
    {

        if ($categories == null or $categories == [])
            return $queryBuilder;

        return $postRepository->findbyCategories($queryBuilder, $categories);
    }

    private function descriptionProcess(QueryBuilder $queryBuilder, ?string $keyWord, postRepository $postRepository): QueryBuilder
    {
        if ($keyWord == null) {
            return $queryBuilder;
        }

        return $postRepository->findByKeyWord($queryBuilder, $keyWord);
    }

    private function makeRender(PaginationInterface $posts, ?string $error, FormInterface $form, PDFService $PDFService,CSVService $CSVService,XMLService $XMLService): Response
    {
        $render = $this->render('post/allPosts.html.twig', [
            'controller_name' => 'categoryController', 'pagination' => $posts, 'error' => $error, 'form' => $form->createView(),
        ]);

        if ($form->getClickedButton() === $form->get('exportPDF')) {
            $render = $this->render('post/allPosts.html.twig', [
                'controller_name' => 'categoryController', 'pagination' => $posts, 'error' => $error, 'form' => $form->createView(),
            ]);
            $pdf = $PDFService->createPDF_FromRequest($posts->getItems());
            dd($pdf);
        }

        if ($form->getClickedButton() === $form->get('exportXML')) {
            $render = $this->render('post/allPosts.html.twig', [
                'controller_name' => 'categoryController', 'pagination' => $posts, 'error' => $error, 'form' => $form->createView(),
            ]);
            $xml_posts = new SimpleXMLElement("<?xml version=\"1.0\"?><posts></posts>");
            $XMLService->createXML_FromRequest($posts->getItems());
            die();


        }

        if ($form->getClickedButton() === $form->get('exportCSV')) {
            $render = $this->render('post/allPosts.html.twig', [
                'controller_name' => 'categoryController', 'pagination' => $posts, 'error' => $error, 'form' => $form->createView(),
            ]);
            echo $CSVService->createCSV_FromRequest($posts->getItems());
            die();
        }

        return $render;
    }


}