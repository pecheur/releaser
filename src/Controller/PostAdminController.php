<?php

namespace App\Controller;

use App\Exception\PostRepositoryException;
use App\Form\PostType;
use App\Repository\CategoryRepository;
use App\Repository\PostRepository;
use App\Repository\UserRepository;
use App\Service\MailService;
use DateTime;
use DateTimeZone;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/post")
 */
class PostAdminController extends AbstractController
{

    /**
     * @Route("/new", name="admin_makePost")
     */
    public function add(Request $request, CategoryRepository $cr, PostRepository $pr, MailerInterface $mailer, MailService $mailService, UserRepository $userRepository)
    {
        $error = null;
        $form = $this->createForm(PostType::class, [$cr->findAll(), null]);
        $form->handleRequest($request);
        if (!$form->isSubmitted() || !$form->isValid()) {
            return $this->make_render($form, $error, false);
        }
        $date = $form->getData()['date'];
        $description = $form->getData()['description'];
        $categories = $form->getData()['categories'];
        if ($categories == null) {
            $error = 'renseigner au moins une catégorie !';
            return $this->make_render($form, $error, false);
        }
        if ($date >= new \Datetime()) {
            $error = 'Vous ne pouvez pas ajouter un poste à une date ultérieur à aujourd\'hui';
            return $this->make_render($form, $error, false);
        }
        try {
            $post = $pr->add(DateTime::createFromFormat('d/m/Y H:i', $date, new DateTimeZone('Europe/Paris')), $description, $categories, $this->getUser());
            $mailService->sendMailPost($mailer, $post, $userRepository);
        } catch (PostRepositoryException $e) {
            return $this->make_render($form, $e->getMessage(), false);
        }
        return $this->redirectToRoute('posts');
    }

    public function make_render($form, $error, $delete)
    {
        return $this->render('admin/addPost.html.twig', [
            'controller_name' => 'PostAdminController',
            'form' => $form->createView(),
            'delete' => $delete,
            'error' => $error,
        ]);
    }

    /**
     * @Route("/{idPost}/modify", name="admin_modifyPost")
     */
    public function modify(Request $request, CategoryRepository $cr, PostRepository $pr, string $idPost, string $error = null, MailerInterface $mailer, MailService $mailService, UserRepository $userRepository)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $post = $pr->find($idPost);
        if ($post == null) {
            throw new NotFoundHttpException("Post not found");
        }
        if ($post->getAuthor() != $this->getUser()) {
            throw new AccessDeniedHttpException("You cant modify this post");
        }
        $form = $this->createForm(PostType::class, [$cr->findAll(), $post]);
        $form->handleRequest($request);

        if (!$form->isSubmitted() || !$form->isValid()) {
            for ($i = 0; $i < count($form->get('categories')); $i++) {
                if (in_array($form->getViewData()[0][$i], $post->getCategories()->getValues())) {
                    $form->get('categories')->get($i)->setData(true);
                }
            }
            return $this->make_render($form, $error, true);
        }
        try {
            $post = $pr->update(DateTime::createFromFormat('d/m/Y H:i', $form->getData()['date'], new DateTimeZone('Europe/Paris')), $form->getData()['description'], $form->getData()['categories'], $post, $this->getUser());
            if ($post != null) {
                $mailService->sendMailPost($mailer, $post, $userRepository);
            }
        } catch (PostRepositoryException $e) {
            return $this->make_render($form, $e->getMessage(), true);
        }
        return $this->redirectToRoute('posts');
    }

    /**
     * @Route("/{idPost}/delete", name="admin_deletePost", methods={"GET"})
     */
    public function delete(PostRepository $pr, string $idPost)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $post = $pr->find($idPost);
        if ($post == null) {
            throw new AccessDeniedHttpException("You cant modify this post");
        }
        $pr->delete($pr->find($idPost));

        return $this->redirectToRoute('posts');
    }
}
