<?php

namespace App\Controller;

use App\Form\EMLFileType;
use App\Form\MakeUserType;
use App\Repository\CategoryRepository;
use App\Repository\PostRepository;
use App\Repository\UserRepository;
use App\Service\EMLService;
use App\Service\LDAPService;
use App\Service\MailService;
use App\Service\UserService;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserAdminController extends AbstractController
{
    /**
     * @Route("/admin/user/new", name="user")
     * @throws TransportExceptionInterface
     */
    public function user(Request $request, UserRepository $userRepository, UserPasswordEncoderInterface $passwordEncoder, MailerInterface $mailer, MailService $mService, UserService $uService,UrlGeneratorInterface $router): Response
    {

        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $isCreated = false;
        $error = null;
        $form = $this->createForm(MakeUserType::class);
        $form->handleRequest($request);

        if (!$form->isSubmitted() || !$form->isValid()) {
            return $this->makeRender($isCreated, $error, $form);
        }
        $key = sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
        $user = $uService->makeUserFromAForm($form, $key);
        try {
            $userRepository->isUnique($user);
        } catch (Exception $e) {
            return $this->makeRender(false, $e->getMessage(), $form);
        }
        $userRepository->add($user);
        $isCreated = true;
        $mService->sendMessageInscription($mailer, $user,$router,$key);

        return $this->makeRender($isCreated, null, $form);
    }

    private function makeRender(bool $isCreated, ?string $error, FormInterface $form): Response
    {
        return $this->render('admin/makeUser.html.twig', [
            'controller_name' => 'AdminController',
            'form' => $form->createView(),
            'isCreated' => $isCreated,
            'error' => $error
        ]);
    }

    /**
     * @Route("/admin/user/manage", name="users", methods={"GET"})
     */
    public function users(UserRepository $userRepository): Response
    {
        //users
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $users = $userRepository->findBy(array(), array("username" => "ASC"));

        return $this->render('admin/manageUser.html.twig', [
            'controller_name' => 'AdminController',
            'users' => $users
        ]);
    }

    /**
     * @Route("/admin/user/changeStatus/{id}", name="changeStatusUser", methods={"GET"})
     */
    public function changeStatusUser(UserRepository $userRepository, string $id): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $userRepository->changeStatus($userRepository->find($id));

        return $this->redirectToRoute('users');
    }

    /**
     * @Route("/admin/addPostFromEmail/", name="addPostFromEmail")
     */
    public function addPostFromEmail(Request $request, EMLService $emlService, UserRepository $userRepository, PostRepository $postRepository, CategoryRepository $categoryRepository, LDAPService $LDAPService, UserService $userService): Response
    {
        $error = null;
        $result = null;
        $form = $this->createForm(EMLFileType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $file = $form['EMLFiles']->getData();
            $result = $emlService->EMLFilesProcces($file, $userRepository, $postRepository, $categoryRepository, $LDAPService, $userService);
        }

        return $this->render('admin/uploadEML.html.twig', [
            'controller_name' => 'AdminController',
            'form' => $form->createView(),
            'error' => $error,
            'result' => $result
        ]);
    }

}
