<?php

namespace App\Controller;

use App\Entity\Category;
use App\Form\CategoryType;
use App\Repository\CategoryRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/category")
 */
class CategoryAdminController extends AbstractController
{
    /**
     * @Route("/new", name="admin_makeCategory")
     */
    public function add(Request $request, CategoryRepository $cr, string $error = null)
    {
        $category = new Category();
        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);

        if (!$form->isSubmitted() || !$form->isValid()) {
            return $this->render('admin/addCategory.html.twig', [
                'controller_name' => 'AdminController',
                'form' => $form->createView(),
                'delete' => null,
                'error' => $error,
            ]);
        }
        $cr->add($category);
        return $this->redirectToRoute('categories');
    }

    /**
     * @Route("/{idCategory}/delete", name="admin_deleteCategory", methods={"GET"})
     */
    public function delete(Request $request, CategoryRepository $cr, string $idCategory)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        if ($cr->find($idCategory)->getPosts()->getValues() != []) {
            $error = "Des posts appartiennent à cette catégorie !";

            return $this->modify($request, $cr, $idCategory, $error);
        }

        $cr->delete($cr->find($idCategory));

        return $this->redirectToRoute('categories');
    }

    /**
     * @Route("/{idCategory}/modify", name="admin_modifyCategory")
     */
    public function modify(Request $request, CategoryRepository $cr, string $idCategory, string $error = null)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $category = $cr->find($idCategory);
        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);

        if (!$form->isSubmitted() || !$form->isValid()) {
            return $this->render('admin/addCategory.html.twig', [
                'controller_name' => 'AdminController',
                'form' => $form->createView(),
                'delete' => true,
                'error' => $error,
            ]);
        }

        $cr->add($category);
        return $this->redirectToRoute('categories');
    }

}
