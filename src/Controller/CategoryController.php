<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Post;
use App\Form\DateBetweenFormType;
use App\Repository\CategoryRepository;
use DateTime;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class CategoryController extends AbstractController
{

    /**
     * @Route("/categories", name="categories", methods={"GET"})
     */
    public function categories(PaginatorInterface $paginator, Request $request, CategoryRepository $categoryRepository): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        $categories = $categoryRepository->findBy(array(), array('name' => 'ASC'));

        $pagination = $paginator->paginate(
            $categories, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            30 /*limit per page*/
        );

        return $this->render('category/categories.html.twig', [
            'controller_name' => 'CategoryController', 'pagination' => $pagination,
        ]);
    }

    /**
     * @Route("/category/{categoryID}/posts", name="postsForACategory")
     */
    public function postsForACategory(Request $request, string $categoryID, PaginatorInterface $paginator, CategoryRepository $categoryRepository): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');

        $error = null;
        $posts = $this->getDoctrine()->getRepository(Category::class)->findby(array('id' => $categoryID));
        $posts = $posts[0]->getPosts()->getValues();
        $form = $this->createForm(DateBetweenFormType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $dateStart = DateTime::createFromFormat('d/m/Y H:i', $form->getData()['dateStart']);
            $dateEnd = DateTime::createFromFormat('d/m/Y H:i', $form->getData()['dateEnd']);
            if (!$dateStart || !$dateEnd) {
                $error = "Veuillez renseignez les deux champs";
            } else {
                if ($dateStart > $dateEnd) {
                    $error = "La date debut doit étre anterieur a la date fin !";
                } else {
                    $posts = $this->getDoctrine()->getRepository(Post::class)->findBetween2Dates($categoryID, $dateStart, $dateEnd);
                    $error = null;
                }
            }
        }
        $category = $categoryRepository->findBy(array('id' => $categoryID));
        $pagination = $paginator->paginate(
            $posts, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            10 /*limit per page*/
        );
        return $this->render('post/posts.html.twig', [
            'controller_name' => 'CategorylController', 'form' => $form->createView(), 'pagination' => $pagination, 'error' => $error, 'category' => $category,
        ]);
    }


}
