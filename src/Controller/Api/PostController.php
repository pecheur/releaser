<?php

namespace App\Controller\Api;

use App\Entity\Post;
use App\Repository\PostRepository;
use Exception;
use Symfony\Component\Security\Core\Security;


class PostController
{

    private $security;
    private $postRepository;

    public function __construct(Security $security, PostRepository $postRepository)
    {
        $this->security = $security;
        $this->postRepository = $postRepository;
    }

    public function __invoke(Post $data)
    {
        $data->setAuthor($this->security->getUser());
        if ($data->getCategories()[0] == []) {
            throw new Exception("category is not nullable !!");
        }
        if (!$this->postRepository->isUnique($data)) {
            throw new Exception("Post Already exist !!");
        }
        return $data;
    }
}
