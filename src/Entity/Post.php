<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PostRepository")
 * @ApiResource(
 *     normalizationContext={"groups"={"API:POST:GET"}},
 *       attributes={
 *      "order"={"date":"DESC"}
 *     },
 *     collectionOperations={
 *         "get",
 *         "post"={
 *              "security"="is_granted('ROLE_ADMIN')",
 *              "controller"=App\Controller\Api\PostController::class
 *          }
 *     },
 *     itemOperations={
 *         "get",
 *     }
 * )
 * @ApiFilter(SearchFilter::class,
 *     properties={"author" : "exact"})
 */
class Post
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups("API:POST:GET")
     */
    private $id;

    /**
     *
     * @ORM\Column(type="datetime", nullable=false)
     * @Groups("API:POST:GET")
     */
    private $date;

    /**
     *
     * @ORM\Column(type="text", nullable=false)
     * @Groups("API:POST:GET")
     *
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     * @Groups("API:POST:GET")
     */
    private $author;
    /**
     * @Groups("API:POST:GET")
     * @ORM\ManyToMany(targetEntity=Category::class, mappedBy="posts")
     */
    private $categories;

    public function __construct()
    {
        $this->categories = new ArrayCollection();
        $this->author = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getDescription();
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @return Collection|Category[]
     */
    public function getCategories(): Collection
    {
        return $this->categories;
    }


    public function addCategory(Category $category): self
    {
        if (!$this->categories->contains($category)) {
            $this->categories[] = $category;
            $category->addPost($this);
        }

        return $this;
    }

    public function removeCategory(Category $category): self
    {
        if ($this->categories->contains($category)) {
            $this->categories->removeElement($category);
            $category->removePost($this);
        }
        return $this;
    }

    public function getAuthor(): ?User
    {
        return $this->author;
    }

    public function setAuthor(?User $author): self
    {
        $this->author = $author;

        return $this;
    }


}
