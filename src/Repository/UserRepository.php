<?php

namespace App\Repository;

use App\Entity\User;
use App\Exception\UserException;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use function get_class;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository implements PasswordUpgraderInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     */
    public function upgradePassword(UserInterface $user, string $newEncodedPassword): void
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', get_class($user)));
        }

        $user->setPassword($newEncodedPassword);
        $user->ClearGuid();
        $this->_em->persist($user);
        $this->_em->flush();
    }

    public function add(UserInterface $user): UserInterface
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', get_class($user)));
        }

        $this->_em->persist($user);
        $this->_em->flush();

        return $user;
    }

    public function isUnique(UserInterface $user): bool
    {
        if ($this->findOneBy(array("email" => $user->getEmail()))) {
            throw new UserException("Email déjà éxistante");
        }

        if ($this->findOneBy(array("username" => $user->getUsername()))) {
            throw new UserException("Username déjà éxistant");
        }

        return true;
    }

    public function changeEmail(UserInterface $user, string $email): bool
    {
        if ($this->findBy(array('email' => $email)) == null) {
            $user->setEmail($email);
            $this->_em->persist($user);
            $this->_em->flush();

            return true;
        }

        return false;
    }

    public function changeStatus(UserInterface $user): UserInterface
    {
        $user->setIsDeleted(!$user->getIsDeleted());
        $this->_em->persist($user);
        $this->_em->flush();

        return $user;
    }

    public function LDAPTOBDD(string $username,string $email): ?UserInterface
    {
        $user = $this->findOneBy(array("username" => $username));
        if($user != null && $email != $user->getEmail() && $email != '' && $email != null){
            $user->setEmail($email);
            $this->_em->persist($user);
            $this->_em->flush();
        }
        return $user;
    }

    public function getUserToNotify(): ?array
    {
        return $this->findBy(array("isNotify" => true, "isDeleted" => false));
    }

    public function changeIsNotify(UserInterface $user): bool
    {
        $user->setIsNotify(!$user->getIsNotify());
        $this->_em->persist($user);
        $this->_em->flush();
        return $user->getIsNotify();
    }
}