<?php

namespace App\Repository;

use App\Entity\Post;
use App\Exception\PostRepositoryException;
use DateTime;
use DateTimeZone;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @method Post|null find($id, $lockMode = null, $lockVersion = null)
 * @method Post|null findOneBy(array $criteria, array $orderBy = null)
 * @method Post[]    findAll()
 * @method Post[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PostRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Post::class);
    }

    /**
     * @return Post[] Returns an array of Version objects
     * Pas typé car peuvent etre des datetimes ou des booleans
     */
    public function findbyBetween2Dates($dateStart, $dateEnd): QueryBuilder
    {
        $qb = $this->createQueryBuilder('p');
        if (!$dateStart or !$dateEnd) {
            return $qb;
        }

        $qb->where('p.date BETWEEN :dateStart AND :dateEnd')
            ->setParameter('dateStart', $dateStart->format('Y-m-d H:i:s'))
            ->setParameter('dateEnd', $dateEnd->format('Y-m-d H:i:s'));

        return $qb;
    }

    public function findbyCategories(QueryBuilder $query, array $categories): QueryBuilder
    {
        $condition = '';
        $condition = $condition . 'posts_categories.id = ' . $categories[0]->getId();
        unset($categories[0]);
        foreach ($categories as $category) {
            $condition = $condition . ' OR posts_categories.id = ' . $category->getId();
        }
        $query->join('p.categories', 'posts_categories', 'WITH', $condition);

        return $query;
    }

    public function findbyCategoryID(string $categoryID): array
    {
        $qb = $this->createQueryBuilder('p');
        $qb->join('p.categories', 'posts_categories', 'WITH', 'posts_categories.id = ' . $categoryID);
        $qb->orderBy('p.date', 'DESC');

        return $qb->getQuery()->getResult();
    }

    public function findByKeyWord(QueryBuilder $qb, string $keyword): QueryBuilder
    {

        $qb->andWhere('p.description LIKE LOWER(:keyword)');
        $qb->setParameter('keyword', '%' . $keyword . '%');

        return $qb;
    }

    public function add(DateTime $date, string $description, array $categories, UserInterface $user): Post
    {
        $em = $this->getEntityManager();
        $post = new Post();
        $post->setDescription($description);
        if ($date >= new DateTime("now", new DateTimeZone('Europe/Paris'))) {
            throw new PostRepositoryException("Vous ne pouvez pas ajouter un poste à une date ultérieur à aujourd'hui");
        }

        $post->setDate($date);
        $post->setAuthor($user);

        foreach ($categories as $categorie) {
            $post->addCategory($categorie);
        }

        if (!$this->isUnique($post)) {
            $em->clear();
            throw new PostRepositoryException("post deja existant ! (descriptions et categories)");
        }

        $em->persist($post);
        $em->flush();
        return $post;
    }

    public function isUnique(Post $post): bool
    {
        $posts = $this->findAll();
        foreach ($posts as $p) {
            if ($p->getCategories()->count() == $post->getCategories()->count() && [array_diff($p->getCategories()->toArray(), $post->getCategories()->toArray())][0] == [] && $p->getDescription() == $post->getDescription()) {
                return false;
            }
        }

        return true;
    }

    public function addPost(Post $post): Post
    {
        $em = $this->getEntityManager();
        if (!$this->isUnique($post)) {
            $em->clear();
            throw new PostRepositoryException("post deja existant ! (descriptions et categories)");
        }
        $em->persist($post);
        $em->flush();

        return $post;
    }

    public function update(DateTime $date, string $description, array $categories, Post $post, UserInterface $user): ?Post
    {
        $em = $this->getEntityManager();
        if ($categories == []) {
            throw new PostRepositoryException("Il faut choisir au moins une catégorie !");
        }
        if ($date >= new DateTime("now", new DateTimeZone('Europe/Paris'))) {
            throw new PostRepositoryException("Vous ne pouvez pas ajouter un poste à une date ultérieur à aujourd'hui");
        }
        $postupdated = new Post();
        $postupdated->setDescription($description);
        $postupdated->setDate($date);
        $postupdated->setAuthor($user);
        foreach ($categories as $categorie) {
            $postupdated->addCategory($categorie);
        }

        if ($post->getDescription() == $description && $categories == $post->getCategories()) {
            $em->remove($post);
            $em->persist($postupdated);
            $em->flush();
            return null;
        }

        $result = $this->isUnique($postupdated);
        if (!$result) {
            $em->clear();
            throw new PostRepositoryException("post deja existant ! (descriptions et categories)");
        }

        $em->remove($post);
        $em->persist($postupdated);
        $em->flush();
        return $postupdated;
    }

    public function delete(Post $post)
    {
        $em = $this->getEntityManager();
        $em->remove($post);
        $em->flush();
    }
}
