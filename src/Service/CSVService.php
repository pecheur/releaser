<?php


namespace App\Service;


use DOMDocument;

class CSVService
{
    public function createCSV_FromRequest(array $posts)
    {
       if($posts > 0){
            $delimiter = ",";
            $filename = "posts_" . date('Y-m-d') . ".csv";

            //create a file pointer
            $f = fopen('php://memory', 'w');

            //set column headers
            $fields = array('ID', 'Categories', 'Description', 'Author', 'Date');
            fputcsv($f, $fields, $delimiter);
            //output each row of the data, format line as csv and write to file pointer

            foreach ($posts as $post) {
                $arrayWithoutTheFirstElement = array_slice($post->getCategories()->toArray(), 1);
                $categories = $post->getCategories()->first()->getName() ;
                foreach ( $arrayWithoutTheFirstElement as $category) {
                    $categories = $categories.'/'.$category->getName();
                }
                $lineData = array($post->getId(), $categories, $post->getDescription(), $post->getAuthor()->getUsername(), $post->getDate()->format('d/m/Y H:i'));
                fputcsv($f, $lineData, $delimiter);
            }
        }

        fseek($f, 0);
        header('Content-Type: text/csv');
        header('Content-Disposition: attachment; filename="'.$filename.'";');
        fpassthru($f);
    }
}