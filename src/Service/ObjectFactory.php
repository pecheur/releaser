<?php


namespace App\Service;


use App\Entity\Post;
use App\Exception\ObjectFactoryException;


class ObjectFactory
{
    public function create(string $class, array $param): ?Post
    {
        switch ($class) {
            case Post::class:
                return $this->makePost($param);
        }
        throw new ObjectFactoryException("type inconnue");

    }

    private function makePost(array $param): Post
    {
        $post = new Post();
        $post->setAuthor($param["user"]);
        foreach ($param["category"] as $category) {
            $post->addCategory($category[0]);
        }
        $post->setDescription($param["description"]);
        $post->setDate($param["date"]);
        return $post;
    }

}