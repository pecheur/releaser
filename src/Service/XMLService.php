<?php


namespace App\Service;



use DOMDocument;
use SimpleXMLElement;

class XMLService
{
    public function createXML_FromRequest(array $data)
    {
        $xmlDoc = new DOMDocument();
        $root = $xmlDoc->appendChild($xmlDoc->createElement("posts"));
        $tabRows = $root->appendChild($xmlDoc->createElement('rows'));

        foreach($data as $post){
            if(!empty($post)){
                $tabPost = $tabRows->appendChild($xmlDoc->createElement('post'));
                $tabPost->appendChild($xmlDoc->createElement("id", $post->getId()));
                $tabPost->appendChild($xmlDoc->createElement("date", $post->getDate()->format('d/m/Y H:i')));
                $tabPost->appendChild($xmlDoc->createElement("author", $post->getAuthor()->getUsername()));
                $tabPost->appendChild($xmlDoc->createElement("description", $post->getDescription()));
                $tabCategories = $tabPost->appendChild($xmlDoc->createElement("Categories"));
                foreach($post->getCategories() as $category){
                    if(!empty($post)){
                        $tabCategory = $tabCategories->appendChild($xmlDoc->createElement('Category'));
                        $tabCategory->appendChild($xmlDoc->createElement("name", $category->getName()));
                    }
                }
            }
        }
        $file_name = str_replace(' ', '_',"posts").'_'.time().'.xml';
        header("Content-Type: text/xml");
        header('Content-Disposition: attachment; filename="'.$file_name.'";');
        //make the output pretty
        echo $xmlDoc->saveXml();
        }
}