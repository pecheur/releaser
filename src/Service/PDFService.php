<?php


namespace App\Service;


use App\Entity\FPDF;


class PDFService
{
    public function createPDF_FromRequest(array $posts): string
    {

        $pdf = new FPDF();
        $pdf->AddPage();
        $pdf->SetFont('Arial', 'B', 16);

        foreach ($posts as $post) {
            $categoriesTXT = '| ';
            foreach ($post->getCategories() as $category) {
                $categoriesTXT = $categoriesTXT . $category->getName() . ' | ';
            }
            $pdf->SetFillColor(200, 220, 255);

            $pdf->Cell(0, 10, $post->getDate()->format('d/m/Y H:i'), 0, 1, '', true);
            $pdf->Cell(0, 10, utf8_decode($categoriesTXT), 0, 1);
            $pdf->MultiCell(0, 7, utf8_decode($post->getDescription()));
            $pdf->Cell(0, 10, utf8_decode('auteur : ' . $post->getAuthor()->getUsername()), 0, 1);
            $pdf->Ln(5);
        }
        $pdf = $pdf->Output('', 'Export.pdf', true);
        return $pdf;

    }
}