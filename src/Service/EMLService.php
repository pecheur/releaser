<?php


namespace App\Service;

use App\Entity\Post;
use App\Entity\User;
use App\Exception\PostRepositoryException;
use App\Repository\CategoryRepository;
use App\Repository\PostRepository;
use App\Repository\UserRepository;
use DateTime;
use Exception;
use PhpMimeMailParser\Parser;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class EMLService
{
    private $aliasCategories;
    private $work;
    private $dontWork;

    public function EMLFilesProcces(array $form, UserRepository $userRepository, PostRepository $postRepository, CategoryRepository $categoryRepository, LDAPService $LDAPService, UserService $userService): array
    {
        $this->aliasCategories = json_decode($_ENV["ALIASCATEGORIES"], true);

        foreach ($form as $file) {
            $this->EMLFileProcces($file, $userRepository, $postRepository, $categoryRepository, $LDAPService, $userService);
        }

        return [$this->dontWork, $this->work];
    }

    public function EMLFileProcces(UploadedFile $file, UserRepository $userRepository, PostRepository $postRepository, CategoryRepository $categoryRepository, LDAPService $LDAPService, UserService $userService)
    {
        if (!$this->checkExtension($file)) return;
        $content = str_replace("\r", "", file_get_contents($file->getPathName()));
        $user = $this->emailProcess($file, $content, $userRepository, $LDAPService, $userService);
        if ($user == null)
            return null;

        $categories = $this->categoryProcess($file, $categoryRepository);
        if ($categories == null)
            return;

        $desc = $this->descriptionProcess($file, $content);
        if ($desc == null)
            return;
        $date = $this->dateProcess($content);
        try {
            $post = $postRepository->addPost($this->makePost($date, $user, $categories, $desc));
            $this->work[] = $file->getClientOriginalName();
        } catch (PostRepositoryException $e) {
            $this->dontWork[] = array($file->getClientOriginalName(), $e->getMessage());
        }


    }

    private function checkExtension(UploadedFile $file): bool
    {
        $extension = $file->getExtension();

        if ($extension != "eml") {
            $this->dontWork[] = array($file->getClientOriginalName(), "fichier " . $extension . " non pris en charge");
            return false;
        }

        return true;
    }

    private function emailProcess(UploadedFile $file, string $content, UserRepository $userRepository, LDAPService $LDAPService, UserService $userService): ?user
    {
        preg_match('/From: .* <(.*)>/', $content, $email);
        $email[1] = quoted_printable_decode($email[1]);
        $user = $userRepository->findBy(array("email" => $email[1]));
        if ($user == null) {
            if (strpos($email[1], '@pecheur.com')) {
                $email[1] = str_replace("@pecheur.com", '', $email[1]);
            }
            $user = $LDAPService->userExist($email[1], $userRepository, $userService);
            if ($user == null) {
                $user = $userRepository->findBy(array("email" => "e.restoy@fugam.lan"))[0];
            }
        }

        return $user;
    }

    private function categoryProcess(string $subject, CategoryRepository $categoryRepository): ?array
    {
        try {

            preg_match('/.*mise en ligne (.*)/i', $subject, $categorieFind);
            $categories[] = $categorieFind[1];

        } catch (Exception $e) {
            return [$categoryRepository->findBy(array("name" => "A TRIER"))];
        }

        if (strstr($categorieFind[1], "/")) {
            $categories = explode("/", $categorieFind[1]);
        }
        if (strstr($categorieFind[1], " + ")) {
            $categories = explode(" + ", $categorieFind[1]);
        }

        $categoriesKey = null;
        foreach ($categories as $cat) {
            foreach ($this->aliasCategories as $key => $value) {
                foreach ($value as $alias) {
                    if ($alias == utf8_encode(strtolower(utf8_decode($cat)))) {
                        $categoriesKey[] = $key;
                        break 2;
                    }
                }
            }
        }
        if ($categoriesKey == null) {
            foreach ($categories as $category) {
                $cat = $categoryRepository->findBy(array('name' => $category));
                if ($cat != null) {
                    $categoriesKey[] = $category;
                }
            }
        }
        if ($categoriesKey == null) {
            return [$categoryRepository->findBy(array("name" => "A TRIER"))];
        }

        foreach ($categoriesKey as $key) {
            $categoriesObject[] = $categoryRepository->findBy(array('name' => $key));
        }
        if ($categoriesObject == null) {
            return [$categoryRepository->findBy(array("name" => "A TRIER"))];
        }
        return $categoriesObject;
    }

    private function descriptionProcess(UploadedFile $file, string $content): ?string
    {

        preg_match('/Content-Transfer-Encoding: quoted-printable(\n)*((.*\n)*)Cordialement/mi', $content, $desc);
        if (!isset($desc[2])) {
            preg_match('/Content-Transfer-Encoding: .*(\n)*((.*\n)*)memo/mi', $content, $desc);
            if (!isset($desc[2])) {
                preg_match('/Content-Transfer-Encoding: .*(\n)*((.*\n)*)--/mi', $content, $desc);
                if (!isset($desc[2])) {
                    preg_match('/Content-Transfer-Encoding: .*(\n)*((.*\n)*)/mi', $content, $desc);
                    if (!isset($desc[2])) {
                        $this->dontWork[] = array($file->getClientOriginalName(), "Description incorrecte");
                        return null;
                    } else {
                        return (utf8_encode($desc[2]));
                    }
                } else {
                    return (utf8_encode($desc[2]));
                }
            } else {
                return (utf8_encode($desc[2]));
            }
        }
        $desc[2] = quoted_printable_decode($desc[2]);
        return $desc[2];
    }

    private function dateProcess(string $content): DateTime
    {
        preg_match('/Date: (.*)/', $content, $date);
        return new DateTime($date[1]);

    }

    private function makePost(DateTime $date, User $user, array $categories, string $description): Post
    {
        $objectFactory = new ObjectFactory();
        return $objectFactory->create(Post::class, array("user" => $user, "category" => $categories, "description" => $description, "date" => $date));
    }

    public function EMLFilesProcessCommand(string $folder, UserRepository $userRepository, PostRepository $postRepository, CategoryRepository $categoryRepository, LDAPService $LDAPService, UserService $userService): array
    {
        $folder = $folder . '\*';
        $folder = glob($folder);
        $this->aliasCategories = json_decode($_ENV["ALIASCATEGORIES"], true);
        $parser = new Parser();


        foreach ($folder as $file) {

            $parser->setPath($file);
            $description = $parser->getMessageBody('text');
            $description = preg_replace('/::.*/is', '', $description);
            $description = preg_replace('/jérémy lair.*/is', '', $description);
            $description = preg_replace('/cordialement.*/is', '', $description);
            $description = preg_replace('/-- \*.*/', '', $description);
            $description = preg_replace('/pecheur.com.*/', '', $description);
            $description = preg_replace('/-- <http.*/', '', $description);
            $date = new DateTime($parser->getHeader('Date'));
            $email = $parser->getAddresses('from')[0]["address"];
            if ($email == "niko.brunet@pecheur.com") {
                $email = "n.brunet@pecheur.com";
            }
            $user = $userRepository->findBy(array("email" => $email[0]));
            $categories = $this->categoryProcess($parser->getHeader('subject'), $categoryRepository);
            if ($categories == null)
                continue;
            if ($user == null) {
                if (strpos($email, '@pecheur.com')) {
                    $email = str_replace("@pecheur.com", '', $email);
                }
                $user = $LDAPService->userExist($email, $userRepository, $userService);
                if ($user == null) {
                    $user = $userRepository->findBy(array("email" => "AdminReleaser@pecheur.com"))[0]; // user par defaut a changer
                }
            }
            try {
                $post = $postRepository->addPost($this->makePost($date, $user, $categories, $description));
                $this->work[] = $parser->getHeader('subject');
            } catch (PostRepositoryException $e) {
                $this->dontWork[] = array($parser->getHeader('subject'), $e->getMessage());
            }

        }

        return ([$this->dontWork, $this->work]);
    }
}