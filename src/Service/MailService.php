<?php

namespace App\Service;

use App\Entity\Post;
use App\Repository\UserRepository;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class MailService
{
    public function sendMessageInscription(MailerInterface $mailer, UserInterface $user,UrlGeneratorInterface $router,string $key): bool
    {

        $email = (new TemplatedEmail())
            ->from('systems@pecheur.com')
            ->to($user->getEmail())
            ->subject('Welcome to Releaser !')
            ->htmlTemplate('mail/inscription.html.twig')
            ->context([
            'user' => $user,
            'url' => $router->generate('registerGUID', ['guid' => $key],UrlGeneratorInterface::ABSOLUTE_URL),
        ]);
        try {
            $mailer->send($email);
            return true;
        } catch (TransportExceptionInterface $e) {
            return false;
        }
    }

    public function sendErrorMail(MailerInterface $mailer,String $error,int $errorCode): bool
    {
        $email = (new Email())
            ->from('systems@pecheur.com')
            ->to($_ENV["ERRORMAILLIST"])
            ->subject('[RELEASER] : LDAP ERROR : '.$errorCode)
            ->text($error);
        try {
            $mailer->send($email);
            return true;
        } catch (TransportExceptionInterface $e) {
            return false;
        }
    }
    public function sendMailPost(MailerInterface $mailer,Post $post,UserRepository $userRepository): bool
    {
        $arrayWithoutTheFirstElement = array_slice($post->getCategories()->toArray(), 1);
        $categories = $post->getCategories()->first()->getName();

        foreach ( $arrayWithoutTheFirstElement as $category) {
            $categories = $categories.' | '.$category->getName();
        }
        $users = $userRepository->getUserToNotify();
        if($users == null){
            return false;
        }
        $email = (new TemplatedEmail());
        foreach ($users as $user) {
            $email->addTo($user->getEmail());
        }
        $email->from("systems@pecheur.com")
            ->subject('['.$post->getAuthor()->getUsername().'] Mise en ligne : '.$post->getCategories()->first() )
            ->htmlTemplate('mail/notify.html.twig')
            ->context([
                'post' => $post,
                'categories' => $categories,
            ]);
        try {
            $mailer->send($email);
            return true;
        } catch (TransportExceptionInterface $e) {
            return false;
        }
    }

}