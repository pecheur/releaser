<?php

namespace App\Service;

use App\Entity\User;
use Symfony\Component\Form\FormInterface;

class UserService
{
    public function makeUserFromAForm(FormInterface $form, string $key): User
    {
        // a mettre dans la factory
        $user = new User();
        $user->setEmail($form->getData()['email']);
        $user->setUsername($form->getData()['username']);
        $user->setGuid($key);
        if ($form->getData()['isAdmin']) {
            $user->setRoles(['ROLE_ADMIN']);
        }
        return $user;
    }


    public function makeUserFromLDAP(string $dn, array $attrs): User
    {
        // a mettre dans la factory
        $user = new User();
        $user->setUsername($attrs["name"][0]);
        $user->setEmail($attrs["mail"][0]);
        $role = [];
        if (strpos($dn, 'OU=Informatique')) {
            $role = ['ROLE_ADMIN'];
        }
        $user->setRoles($role);

        return $user;
    }
}