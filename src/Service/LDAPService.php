<?php


namespace App\Service;


use App\Entity\User;
use App\Repository\UserRepository;
use Exception;
use Symfony\Component\Ldap\Exception\ConnectionException;
use Symfony\Component\Ldap\Ldap;
use Symfony\Component\Mailer\MailerInterface;

class LDAPService
{
    public function login(string $sAMAccountName, string $password, UserRepository $userRepository, UserService $uService, MailService $mailService, MailerInterface $mailer): ?User
    {
        $ldap = Ldap::create('ext_ldap', [
            'host' => $_ENV['LDAP_HOST'],
            'encryption' => 'none',
            'port' => 389,
        ]);
        try {
            $ldap->bind($_ENV['LDAP_CN'], $_ENV['LDAP_PASSWORD']);
            $query = $ldap->query($_ENV['LDAP_DN'], '(sAMAccountName=' . $sAMAccountName . ')');
            $result = $query->execute();
            if (!isset($result[0])) {
                $query = ($ldap->query($_ENV['LDAP_DN'], '(userPrincipalName=' . $sAMAccountName . ')'))->execute();
                if (!isset($query[0])) {
                    return null;
                }
                $result[0] = $query[0];
            }
        } catch (ConnectionException $exception) {
            $mailService->sendErrorMail($mailer, $exception->getMessage(), $exception->getCode());
            return null;
        }
        try {
            $ldap->bind($result[0]->getDn(), $password);
        } catch (Exception $e) {
            return null;
        }

        $user = $userRepository->LDAPTOBDD($result[0]->getAttributes()['name'][0],$result[0]->getAttributes()['mail'][0]);
        if ($user == null) {
            $user = $uService->makeUserFromLDAP($result[0]->getDn(), $result[0]->getAttributes());
            $user = $userRepository->add($user);
        }

        if($user)
        return $user;
    }

    public function userExist(string $sAMAccountName, UserRepository $userRepository, UserService $uService): ?User
    {
        $ldap = Ldap::create('ext_ldap', [
            'host' => $_ENV['LDAP_HOST'],
            'encryption' => 'none',
            'port' => 389,
        ]);
        $ldap->bind($_ENV['LDAP_CN'], $_ENV['LDAP_PASSWORD']);
        $query = $ldap->query($_ENV['LDAP_DN'], '(sAMAccountName=' . $sAMAccountName . ')');
        $result = $query->execute();
        if ($result[0] == null) {
            return null;
        }
        $user = $userRepository->LDAPTOBDD($result[0]->getAttributes()['name'][0],$result[0]->getAttributes()['mail'][0]);
        if ($user == null) {
            $user = $uService->makeUserFromLDAP($result[0]->getDn(), $result[0]->getAttributes());
            $user = $userRepository->add($user);
        }
        return $user;
    }
}