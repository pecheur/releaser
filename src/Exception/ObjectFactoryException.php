<?php


namespace App\Exception;


use Exception;

class ObjectFactoryException extends Exception
{

    /**
     * UserException constructor.
     */
    public function __construct(String $message)
    {
        parent::__construct($message);
    }
}